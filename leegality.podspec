Pod::Spec.new do |s|
  
  s.name         = "leegality"

  s.version      = "1.0.0"

  s.summary      = "A short description of leegality."

  s.homepage     = "https://leegality.com"

  s.license      = "MIT"

  s.author       = { "Prakhar Agrawal" => "prakhar@leegality.com" }

  s.platform     = :ios, "12.0"

  s.source       = { :git => "https://leegality@bitbucket.org/leegality/ios.git", :tag => "1.0.0" }

  s.source_files = "leegality", "leegality/**/*.{h,m,swift}"

end
