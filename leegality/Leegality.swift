//
//  Leegality.swift
//  leegality
//
//  Created by Prakhar Agrawal on 20/09/18.
//  Copyright © 2018 Grey Swift Private Limited. All rights reserved.
//

import Foundation
import UIKit
import WebKit

public protocol LeegalityProtocol {
    func getResult(value : [String : String])
}

public class Leegality: UIViewController, WKNavigationDelegate {
    
    public var url: String!
    public var otp: String!
    public var mobile: String!
    public var delegate: LeegalityProtocol?
    public var loadSpinner: UIActivityIndicatorView!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        let packageName = Bundle.main.bundleIdentifier!
        let requestUrl = URL(string: url)!
        var apiURL = "https://"+requestUrl.host!+"/request/verify?device=ios&requestUrl="
        apiURL = apiURL+url+"&otp="+otp
        apiURL = apiURL+"&username="+mobile+"&package="+packageName
        let finalAPIURL = URL(string: apiURL)!
        let requestObj = URLRequest(url: finalAPIURL)
        let webView:WKWebView = WKWebView(frame: UIScreen.main.bounds)
        loadSpinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        let transform:CGAffineTransform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        loadSpinner.transform = transform;
        loadSpinner.center = view.center
        webView.navigationDelegate = self
        webView.load(requestObj)
        self.view.addSubview(webView)
        self.view.addSubview(loadSpinner)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public func webViewDidStartLoad(_ : WKWebView) {
        loadSpinner.startAnimating()
    }
    
    public func webViewDidFinishLoad(_ : WKWebView) {
        loadSpinner.stopAnimating()
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let scheme = navigationAction.request.url?.scheme
        if scheme == "leegality" {
            let string = navigationAction.request.url?.absoluteString
            let strings = string!.components(separatedBy: "://")[1].components(separatedBy: ":")
            var dict = [String: String]()
            dict[strings[0]] = strings[1].removingPercentEncoding!
            delegate?.getResult(value: dict)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
